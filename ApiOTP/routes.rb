add_route "GET", url: "/totp/:secret", method: "totp", controller: "GeneratorController"
add_route "GET", url: "/hotp/:secret/:count", method: "hotp", controller: "GeneratorController"
